﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhosNext.Api.ViewModels;

namespace WhosNext.Api.Utilities
{
    public class PagingHelper<TModel>
    {
        public static PagingViewModel<TModel> Paging(IQueryable<TModel> model, PagingViewModel<TModel> viewModel)
        {
            Func<TModel, object> sorter = TryBuildSortingFunction(viewModel.SortProperty);

            viewModel.TotalCount = model.Count();

            if (viewModel.PageNumber == 0)
            {
                if (viewModel.OrderByDescending)
                {
                    viewModel.Contents = model
                                        .OrderByDescending(sorter)
                                        .ToList();
                }
                else
                {
                    viewModel.Contents = model
                                        .OrderBy(sorter)
                                        .ToList();
                }
            }
            else
            {
                if (viewModel.OrderByDescending)
                {
                    viewModel.Contents = model
                                    .OrderByDescending(sorter)
                                    .Skip((viewModel.PageNumber - 1) * viewModel.NumberOfContentPerPage)
                                    .Take(viewModel.NumberOfContentPerPage)
                                    .ToList();
                }
                else
                {
                    viewModel.Contents = model
                                    .OrderBy(sorter)
                                    .Skip((viewModel.PageNumber - 1) * viewModel.NumberOfContentPerPage)
                                    .Take(viewModel.NumberOfContentPerPage)
                                    .ToList();
                }
            }

            return viewModel;
        }

        private static Func<TModel, object> TryBuildSortingFunction(string property)
        {
            if (typeof(TModel).GetProperty(property) != null)
            {
                return x => x.GetType().GetProperty(property).GetValue(x, null);
            }
            return null;
        }
    }
}
