﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhosNext.Api.DAL;

namespace WhosNext.Api.ViewModels
{
    public class TreatViewModel
    {
        public int Id { get; set; }
        public int AskedTo { get; set; }
        public string AskedToName { get; set; }
        public int AskedBy { get; set; }
        public string AskedByName { get; set; }
        public DateTime Deadline { get; set; }
        public string Reason { get; set; }
        public string Heading { get; set; }
        public TreatStage Confirmation { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ConfirmationDate { get; set; }
        public DateTime? CompleteDate { get; set; }
    }
}
