﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhosNext.Api.ViewModels
{
    public class AskTreatViewModel
    {
        public int AskedTo { get; set; }
        public DateTime Deadline { get; set; }
        public string Reason { get; set; }

        public decimal TimeZoneOffset { get; set; }
    }
}
