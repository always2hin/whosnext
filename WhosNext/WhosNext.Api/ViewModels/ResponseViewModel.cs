﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhosNext.Api.ViewModels
{
    public class ResponseViewModel
    {
        public List<string> Messages;
        public List<string> ErrorDescriptions;
        public bool Success;

        public ResponseViewModel()
        {
            Success = true;
            Messages = new List<string>();
            ErrorDescriptions = new List<string>();
        }

        public ResponseViewModel(string message)
        {
            Success = true;
            Messages = new List<string> { message };
            ErrorDescriptions = new List<string>();
        }

        public ResponseViewModel AddMessage(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                Messages.Add(message);
            }

            return this;
        }

        public ResponseViewModel AddErrorDescription(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                ErrorDescriptions.Add(message);
            }

            return this;
        }

        public bool CheckSuccess()
        {
            return Success;
        }

        public ResponseViewModel Succeed()
        {
            Success = true;
            return this;
        }
        public ResponseViewModel Failure()
        {
            Success = false;
            return this;
        }
    }

    public class ResponseViewModelWithEntity<TViewModel> : ResponseViewModel
    {
        public TViewModel Entity { get; set; }

        public ResponseViewModelWithEntity() : base()
        {
        }

        public ResponseViewModelWithEntity(string message) : base(message)
        {
        }
    }

    public class ResponseViewModelWithAdditionInformation<TViewModel, TAdditionalInformatinModel> : ResponseViewModelWithEntity<TViewModel>
    {
        public TAdditionalInformatinModel AdditionalInformation { get; set; }

        public ResponseViewModelWithAdditionInformation() : base()
        {
        }

        public ResponseViewModelWithAdditionInformation(string message) : base(message)
        {
        }
    }
}
