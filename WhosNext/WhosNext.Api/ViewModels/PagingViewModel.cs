﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhosNext.Api.ViewModels
{
    public class PagingViewModel<TViewModel>
    {
        public List<TViewModel> Contents { get; set; }
        public int PageNumber { get; set; }
        public int TotalCount { get; set; }
        public int NumberOfContentPerPage { get; set; }
        public string SortProperty { get; set; }
        public bool OrderByDescending { get; set; }
        public string SearchText { get; set; }
    }
}
