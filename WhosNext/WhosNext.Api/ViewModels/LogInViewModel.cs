﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhosNext.Api.ViewModels
{
    public class LogInViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
