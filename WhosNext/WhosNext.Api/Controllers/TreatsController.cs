﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WhosNext.Api.DAL;
using WhosNext.Api.Manager;
using WhosNext.Api.ViewModels;

namespace WhosNext.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TreatsController : BaseController
    {
        public TreatsController(WhosNextDbContext db) : base(db)
        {

        }

        [HttpPost]
        [Route("addtreat")]
        public IActionResult AddTreat([FromBody]Treat treat)
        {
            ResponseViewModel response = new ResponseViewModel();

            if (treat == null || treat.Deadline < DateTime.UtcNow || treat.Reason == null)
            {
                response.Failure();
            }

            if (response.Success)
            {
                using (TreatsManager tm = new TreatsManager(db))
                {
                    tm.AskTreat(UserEmail, treat, response);
                }
            }

            return new ObjectResult(response);
        }

        [HttpGet]
        [Route("gettreatrequests")]
        public IActionResult GetTreatRequests(PagingViewModel<TreatViewModel> pagingViewModel)
        {
            ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int> response = new ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int>();

            using (TreatsManager tm = new TreatsManager(db))
            {
                tm.GetTreatRequests(UserEmail, response, pagingViewModel);
            }

            return new ObjectResult(response);
        }

        [HttpPost]
        [Route("confirmtreat")]
        public IActionResult ConfirmTreat([FromBody]int treatId)
        {
            ResponseViewModel response = new ResponseViewModel();

            using (TreatsManager tm = new TreatsManager(db))
            {
                tm.ConfirmTreat(treatId, UserEmail, response);
            }

            return new ObjectResult(response);
        }

        [HttpPost]
        [Route("movetotrash")]
        public IActionResult MoveToTrash([FromBody]int treatId)
        {
            ResponseViewModel response = new ResponseViewModel();

            using (TreatsManager tm = new TreatsManager(db))
            {
                tm.TrashTreat(treatId, UserEmail, response);
            }

            return new ObjectResult(response);
        }

        [HttpGet]
        [Route("gettrash")]
        public IActionResult GetTrash(PagingViewModel<TreatViewModel> pagingViewModel)
        {
            ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int> response = new ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int>();

            using (TreatsManager tm = new TreatsManager(db))
            {
                tm.GetTrash(UserEmail, response, pagingViewModel);
            }

            return new ObjectResult(response);
        }

        [HttpGet]
        [Route("gettreats")]
        public IActionResult GetTreats(PagingViewModel<TreatViewModel> pagingViewModel)
        {
            ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int> response = new ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int>();

            using (TreatsManager tm = new TreatsManager(db))
            {
                tm.GetTreats(UserEmail, response, pagingViewModel);
            }

            return new ObjectResult(response);
        }

        [HttpPost]
        [Route("completetreat")]
        public IActionResult CompleteTreat([FromBody]int treatId)
        {
            ResponseViewModel response = new ResponseViewModel();

            using (TreatsManager tm = new TreatsManager(db))
            {
                tm.CompleteTreat(treatId, UserEmail, response);
            }

            return new ObjectResult(response);
        }

        [HttpGet]
        [Route("gettreathistory")]
        public IActionResult GetTreatHistory(PagingViewModel<TreatViewModel> pagingViewModel)
        {
            ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int> response = new ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int>();

            using (TreatsManager tm = new TreatsManager(db))
            {
                tm.GetTreatHistory(UserEmail, response, pagingViewModel);
            }

            return new ObjectResult(response);
        }
    }
}