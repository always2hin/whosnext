﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WhosNext.Api.DAL;

namespace WhosNext.Api.Controllers
{
    public class BaseController : Controller
    {
        protected WhosNextDbContext db;
        protected IConfiguration Configuration { get; }
        protected string UserEmail
        {
            get
            {
                return HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            }
        }

        public BaseController(WhosNextDbContext db, IConfiguration configuration)
        {
            this.db = db;
            Configuration = configuration;
        }

        public BaseController(WhosNextDbContext db)
        {
            this.db = db;
        }
    }
}