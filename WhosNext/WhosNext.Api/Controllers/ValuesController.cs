﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WhosNext.Api.DAL;

namespace WhosNext.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private WhosNextDbContext db;

        public ValuesController(WhosNextDbContext db)
        {
            this.db = db;
        }

        // GET api/values
        [HttpGet]
        public WhosNextUser Get()
        {
            ClaimsPrincipal user = HttpContext.User;
            string email = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email).Value;

            return db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(email));
        }
    }
}
