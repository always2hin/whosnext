﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WhosNext.Api.DAL;
using WhosNext.Api.Manager;
using WhosNext.Api.ViewModels;

namespace WhosNext.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AuthController : BaseController
    {
        public AuthController(WhosNextDbContext db, IConfiguration configuration) : base(db, configuration)
        {

        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public IActionResult LogIn([FromBody]LogInViewModel logInViewModel)
        {
            ResponseViewModelWithEntity<TokenViewModel> response = new ResponseViewModelWithEntity<TokenViewModel>();

            if (logInViewModel == null || logInViewModel?.Email == null || logInViewModel?.Password == null)
            {
                response.Failure();
            }

            if(response.Success)
            {
                using (AuthManager am = new AuthManager(db, Configuration))
                {
                    am.LogIn(logInViewModel, response);
                }
            }

            return new ObjectResult(response);
        }
    }
}