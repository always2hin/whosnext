﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WhosNext.Api.DAL;
using WhosNext.Api.Manager;
using WhosNext.Api.ViewModels;

namespace WhosNext.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AccountsController : BaseController
    {
        public AccountsController(WhosNextDbContext db) : base(db)
        {

        }

        [HttpPost]
        [Route("signup")]
        [AllowAnonymous]
        public IActionResult SignUp([FromBody]SignUpViewModel signUpViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();

            if (signUpViewModel.Name == null
                || signUpViewModel.Name.Trim().Length == 0
                || signUpViewModel.Email == null 
                || signUpViewModel.Password == null
                || signUpViewModel.Gender == 0)
            {
                response.Failure();
            }
            else
            {
                try
                {
                    DateTime checkValidDate = new DateTime(signUpViewModel.BirthYear, signUpViewModel.BirthMonth, signUpViewModel.BirthDate);
                }
                catch(Exception ex)
                {
                    response.Failure();
                }
            }

            if (response.Success)
            {
                using (AccountsManager am = new AccountsManager(db))
                {
                    am.SignUp(signUpViewModel, response);
                }
            }
            return new ObjectResult(response);
        }

        [HttpGet]
        [Route("friendsbysearch")]
        public IActionResult GetAllFrinedsBySearchText(string searchText)
        {
            ResponseViewModelWithEntity<List<UserViewModel>> response = new ResponseViewModelWithEntity<List<UserViewModel>>();

            using (AccountsManager am = new AccountsManager(db))
            {
                am.GetAllFrinedsBySearchText(UserEmail, searchText, response);
            }

            return new ObjectResult(response);
        }
    }
}