﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhosNext.Api.DAL
{
    public class WhosNextDbContext : DbContext
    {
        public WhosNextDbContext(DbContextOptions<WhosNextDbContext> options) : base(options)
        {

        }

        public DbSet<WhosNextUser> WhosNextUsers { get; set; }
        public DbSet<Treat> Treats { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
