﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WhosNext.Api.DAL
{
    public enum TreatStage
    {
        Trashed = -1,
        Waiting = 0,
        Confirmed = 1,
        Completed = 2
    }

    public class Treat
    {
        public int Id { get; set; }
        public int AskedTo { get; set; }
        [ForeignKey("AskedTo")]
        public WhosNextUser AskedToUser { get; set; }
        public int AskedBy { get; set; }
        [ForeignKey("AskedBy")]
        public WhosNextUser AskedByUser { get; set; }
        public DateTime Deadline { get; set; }
        public string Reason { get; set; }
        public string Heading { get; set; }
        public TreatStage Confirmation { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ConfirmationDate { get; set; }
        public DateTime? CompleteDate { get; set; }
    }
}
