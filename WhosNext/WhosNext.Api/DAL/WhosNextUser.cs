﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhosNext.Api.DAL
{
    public enum GernderType
    {
        Female = 1,
        Male = 2,
        PreferNotToSay = 3
    }

    public class WhosNextUser 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public DateTime BirthDate { get; set; }
        public GernderType Gender  { get; set; }
        public string UserId { get; set; }
    }
}
