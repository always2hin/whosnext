﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhosNext.Api.DAL
{
    public class SignUpViewModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int BirthDate { get; set; }
        public int BirthMonth { get; set; }
        public int BirthYear { get; set; }
        public GernderType Gender { get; set; }
    }
}
