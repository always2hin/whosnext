﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WhosNext.Api.DAL;
using WhosNext.Api.ViewModels;

namespace WhosNext.Api.Manager
{
    public class AccountsManager : BaseManager
    {
        public AccountsManager (WhosNextDbContext db) : base(db)
        {

        }

        public string GetPasswordHash(string email, string password)
        {
            StringBuilder PasswordHash = new StringBuilder();

            using (MD5 md5 = MD5.Create())
            {
                int remover = 2;
                while ( true )
                {
                    if(email.Length == 5)
                    {
                        break;
                    }

                    email = email.Remove(remover, 1);
                    remover += 3;
                    remover %= email.Length;
                }

                for(int i = 0; i< password.Length; i++ )
                {
                    password = password.Insert(i, email);
                    i += 5;
                }

                byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < data.Length; i++)
                {
                    PasswordHash.Append(data[i].ToString("x2"));
                }
            }

            return PasswordHash.ToString();
        }

        public void SignUp(SignUpViewModel signUpViewModel, ResponseViewModel response)
        {
            if (db.WhosNextUsers.Any(u => u.Email.Equals(signUpViewModel.Email)))
            {
                response.Failure().AddErrorDescription("DuplicateEmail");
            }

            if (response.Success)
            {
                WhosNextUser whosNextUser = new WhosNextUser
                {
                    Name = signUpViewModel.Name.Trim(),
                    Email = signUpViewModel.Email.Trim(),
                    BirthDate = new DateTime(signUpViewModel.BirthYear, signUpViewModel.BirthMonth, signUpViewModel.BirthDate),
                    Gender = signUpViewModel.Gender,
                    PasswordHash = GetPasswordHash(signUpViewModel.Email.Trim(), signUpViewModel.Password.Trim()),
                    UserId = GetNewUserId(signUpViewModel)
                };

                db.WhosNextUsers.Add(whosNextUser);

                db.SaveChanges();
            }
        }

        public string GetNewUserId(SignUpViewModel signUpViewModel)
        {
            string userId = String.Empty;

            string baseUserId = userId = signUpViewModel.Name.Trim().Replace(' ', '.').ToLower();

            IQueryable<WhosNextUser> users = db.WhosNextUsers.Where(u => EF.Functions.Like(u.UserId, baseUserId +"%"));

            if (users.Any())
            {
                int baseInt = users.Count();

                while ( true )
                {
                    if(!users.Any(u => u.UserId.Equals(userId)))
                    {
                        break;
                    }

                    userId = baseUserId + baseInt;
                    baseInt++;
                }
            }
            return userId;
        }

        public void GetAllFrinedsBySearchText(string userEmail, string searchText, ResponseViewModelWithEntity<List<UserViewModel>> response)
        {
            response.Entity = GetAllFriends(userEmail).Where(u => searchText == null || searchText .Length ==0 ? true : u.Name.Contains(searchText)).ToList();
        }

        public IQueryable<UserViewModel> GetAllFriends(string userEmail)
        {
            return db.WhosNextUsers
                    .Select(u => new UserViewModel {
                        Id = u.Id,
                        Name = u.Name,
                        Email = u.Email
                    });
        }
    }
}
