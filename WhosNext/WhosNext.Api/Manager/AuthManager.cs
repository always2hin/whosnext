﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WhosNext.Api.DAL;
using WhosNext.Api.ViewModels;

namespace WhosNext.Api.Manager
{
    public class AuthManager : BaseManager
    {
        public AuthManager(WhosNextDbContext db) : base(db)
        {

        }

        public AuthManager(WhosNextDbContext db, IConfiguration configuration) : base(db, configuration)
        {

        }

        public void LogIn(LogInViewModel logInViewModel, ResponseViewModelWithEntity<TokenViewModel> response)
        {
            WhosNextUser user = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(logInViewModel.Email.Trim()));
            string passwordHash = String.Empty;

            if (user == null)
            {
                response.Failure();
            }
            else
            {
                using (AccountsManager am = new AccountsManager(db))
                {
                    passwordHash = am.GetPasswordHash(user.Email, logInViewModel.Password.Trim());
                }

                if (passwordHash.Equals(user.PasswordHash))
                {
                    SymmetricSecurityKey signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWTKey"]));

                    Claim[] claims = new Claim[]
                    {
                        new Claim(JwtRegisteredClaimNames.Email, logInViewModel.Email.Trim())
                    };

                    JwtSecurityToken token = new JwtSecurityToken(
                        issuer: Configuration["WebAppURL"],
                        audience: Configuration["WebAppURL"],
                        expires: DateTime.UtcNow.AddHours(1),
                        claims: claims,
                        signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
                    );

                    response.Succeed();
                    response.Entity = new TokenViewModel
                    {
                        Token = new JwtSecurityTokenHandler().WriteToken(token),
                        Expiration = token.ValidTo
                    };
                }
                else
                {
                    response.Failure();
                }
            }
        }
    }
}
