﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhosNext.Api.DAL;
using WhosNext.Api.ViewModels;
using Microsoft.EntityFrameworkCore;
using WhosNext.Api.Utilities;

namespace WhosNext.Api.Manager
{
    public class TreatsManager : BaseManager
    {
        public TreatsManager(WhosNextDbContext db) : base(db)
        {

        }

        public void AskTreat(string askedByUserEmail, Treat treat, ResponseViewModel response)
        {
            WhosNextUser askedByUser = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(askedByUserEmail));
            WhosNextUser askedToUser = db.WhosNextUsers.Find(treat.AskedTo);

            if (askedToUser == null)
            {
                response.Failure();
            }
            else
            {
                treat.AskedBy = askedByUser.Id;
                treat.Confirmation = TreatStage.Waiting;
                treat.CreateDate = DateTime.UtcNow;
                if (treat.Heading == null)
                {
                    treat.Heading = "";
                }

                db.Treats.Add(treat);
                db.SaveChanges();
            }
        }

        public IQueryable<TreatViewModel> GetTreatsByStage(string userEmail, TreatStage treatStage)
        {
            return db.Treats
                    .Where(t => (t.AskedToUser.Email.Equals(userEmail) || t.AskedByUser.Email.Equals(userEmail)) && t.Confirmation == treatStage)
                    .Select(t => new TreatViewModel
                    {
                        Id = t.Id,
                        Heading = t.Heading,
                        AskedBy = t.AskedBy,
                        AskedByName = t.AskedByUser.Name,
                        AskedTo = t.AskedTo,
                        AskedToName = t.AskedToUser.Name,
                        Reason = t.Reason,
                        Deadline = t.Deadline,
                        Confirmation = t.Confirmation,
                        CreateDate = t.CreateDate,
                        ConfirmationDate = t.ConfirmationDate,
                        CompleteDate = t.CompleteDate
                    });

        }

        public void ConfirmTreat(int treatId, string userEmail, ResponseViewModel response)
        {
            Treat treat = db.Treats.Find(treatId);
            WhosNextUser whosNextUser = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(userEmail));

            if (treat.AskedTo == whosNextUser.Id)
            {
                treat.Confirmation = TreatStage.Confirmed;
                treat.ConfirmationDate = DateTime.UtcNow;
                db.SaveChanges();

                response.Succeed();
            }
            else
            {
                response.Failure();
            }
        }

        public void TrashTreat(int treatId, string userEmail, ResponseViewModel response)
        {
            Treat treat = db.Treats.Find(treatId);
            WhosNextUser whosNextUser = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(userEmail));

            if (treat.AskedTo == whosNextUser.Id || treat.AskedBy == whosNextUser.Id)
            {
                treat.Confirmation = TreatStage.Trashed;
                db.SaveChanges();

                response.Succeed();
            }
            else
            {
                response.Failure();
            }
        }

        public void CompleteTreat(int treatId, string userEmail, ResponseViewModel response)
        {
            Treat treat = db.Treats.Find(treatId);
            WhosNextUser whosNextUser = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(userEmail));

            if (treat.AskedBy == whosNextUser.Id)
            {
                treat.Confirmation = TreatStage.Completed;
                treat.CompleteDate = DateTime.UtcNow;
                db.SaveChanges();

                response.Succeed();
            }
            else
            {
                response.Failure();
            }
        }

        public void GetTreatRequests(string userEmail, ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int> response, PagingViewModel<TreatViewModel> pagingViewModel)
        {
            IQueryable<TreatViewModel> treats = GetTreatsByStage(userEmail, TreatStage.Waiting);

            response.Entity = PagingHelper<TreatViewModel>.Paging(treats, pagingViewModel);

            response.AdditionalInformation = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(userEmail)).Id;
        }

        public void GetTrash(string userEmail, ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int> response, PagingViewModel<TreatViewModel> pagingViewModel)
        {
            int userId = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(userEmail)).Id;

            IQueryable<TreatViewModel> treats = TreatsFilter(GetTreatsByStage(userEmail, TreatStage.Trashed), pagingViewModel, userId);

            response.Entity = PagingHelper<TreatViewModel>.Paging(treats, pagingViewModel);

            response.AdditionalInformation = userId;
        }

        public void GetTreats(string userEmail, ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int> response, PagingViewModel<TreatViewModel> pagingViewModel)
        {
            IQueryable<TreatViewModel> treats = GetTreatsByStage(userEmail, TreatStage.Confirmed);

            response.Entity = PagingHelper<TreatViewModel>.Paging(treats, pagingViewModel);

            response.AdditionalInformation = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(userEmail)).Id;
        }

        public void GetTreatHistory(string userEmail, ResponseViewModelWithAdditionInformation<PagingViewModel<TreatViewModel>, int> response, PagingViewModel<TreatViewModel> pagingViewModel)
        {
            int userId = db.WhosNextUsers.FirstOrDefault(u => u.Email.Equals(userEmail)).Id;

            IQueryable<TreatViewModel> treats = TreatsFilter( GetTreatsByStage(userEmail, TreatStage.Completed), pagingViewModel, userId);

            response.Entity = PagingHelper<TreatViewModel>.Paging(treats, pagingViewModel);

            response.AdditionalInformation = userId;
        }

        private IQueryable<TreatViewModel> TreatsFilter(IQueryable<TreatViewModel> treats, PagingViewModel<TreatViewModel> pagingViewModel, int userId)
        {
            if (!string.IsNullOrWhiteSpace(pagingViewModel.SearchText))
            {
                treats = treats.Where(t => t.Reason.ToLower().Contains(pagingViewModel.SearchText.ToLower())
                            || (t.AskedBy == userId ? t.AskedToName.ToLower().Contains(pagingViewModel.SearchText.ToLower()) : t.AskedByName.ToLower().Contains(pagingViewModel.SearchText.ToLower())));
            }

            return treats;
        }
    }
}
