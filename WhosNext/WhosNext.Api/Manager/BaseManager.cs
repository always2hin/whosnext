﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhosNext.Api.DAL;

namespace WhosNext.Api.Manager
{
    public class BaseManager : IDisposable
    {
        protected WhosNextDbContext db;
        protected IConfiguration Configuration { get; }

        public BaseManager(WhosNextDbContext db)
        {
            this.db = db;
        }

        public BaseManager(WhosNextDbContext db, IConfiguration configuration)
        {
            this.db = db;
            Configuration = configuration;
        }

        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
