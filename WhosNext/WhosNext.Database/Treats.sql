﻿CREATE TABLE [dbo].[Treats]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [AskedTo] INT NOT NULL, 
    [AskedBy] INT NOT NULL, 
    [Deadline] DATETIME NOT NULL, 
    [Reason] NVARCHAR(256) NOT NULL, 
    [Heading] NVARCHAR(32) NOT NULL, 
    [Confirmation] INT NOT NULL,
	[CreateDate] DATETIME NOT NULL,
	[ConfirmationDate] DATETIME,
	[CompleteDate] DATETIME
)
GO

ALTER TABLE Treats
ADD CONSTRAINT FK_AskedTo_WhosNextUsers
FOREIGN KEY (AskedTo) REFERENCES WhosNextUsers(Id);
GO 

ALTER TABLE Treats
ADD CONSTRAINT FK_AskedBy_WhosNextUsers
FOREIGN KEY (AskedBy) REFERENCES WhosNextUsers(Id);
GO