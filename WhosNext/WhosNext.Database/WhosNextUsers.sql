﻿CREATE TABLE [dbo].[WhosNextUsers]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [Name] NVARCHAR(256) NOT NULL, 
    [Email] NVARCHAR(256) NOT NULL, 
    [PasswordHash] NVARCHAR(32) NOT NULL, 
    [BirthDate] DATETIME NOT NULL, 
    [Gender] INT NOT NULL, 
    [UserId] NVARCHAR(256) NOT NULL
)
